# Week 5 Mini-Project: Create a Rust AWS Lambda function and Connect to a database
# Purpose

The purpose of this project is to demonstrate the development and deployment of a Rust-based AWS Lambda function that integrates with Amazon DynamoDB to simulate a lottery draw by assigning a random prize ('win $5', 'win $10' or 'win nothing') to an ID and storing the result in a DynamoDB table.

## Prerequisites

- An AWS account
- Rust and Cargo installed
- AWS CLI installed and configured with necessary permissions
- cargo-lambda installed

## Setup Instructions

### 1. IAM User Setup

- Log in to your AWS account, navigate to the IAM (Identity and Access Management) console, and create a new user
- Attach `AWSLambda_FullAccess`, `IAMFullAccess`, `AmazonDynamoDBFullAccess`, and `AmazonDynamoDBFullAccesswithDataPipeline` policies.
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### 2. DynamoDB Setup

- Go to the AWS Management Console and create a new DynamoDB table named `Lottery` with `lottery_id` as the partition key.

### 3. Project Setup

- Create a new project with `cargo lambda new project_name`.
- Update `main.rs` and `Cargo.toml`.
- Add a `.env` file with your AWS credentials and region, and ensure it's listed in `.gitignore`.
- Export your AWS credentials and region in your terminal session.
- Use the following commands to deploy your Lambda function:
        ```bash
        cargo lambda build --release
        cargo lambda deploy
        ```


### 4. AWS Lambda Setup

- Navigate to the AWS Management Console and open the Lambda service.
- Locate your deployed Lambda function in the dashboard and select it.
- Within the function's detail page, go to the "Configuration" tab.
- Click on "Permissions" in the sidebar menu. Under the "Execution role" section, attach the following IAM policies to the role associated with your Lambda function:`AmazonDynamoDBFullAccess`, `AWSLambdaBasicExecutionRole`.

### 5. API Gateway Setup
- From the AWS Management Console, access the API Gateway service.
- Initiate the creation of a new `REST API` within the API Gateway service.
- For your API, incorporate an `ANY` method.
- Once configured, proceed to deploy your API. You will be provided with an Invoke URL.
- Utilize the terminal to test the functionality of your API Gateway. 

## test the API gateway:
![Function overview](screenshot1.png)

## DynamoDB lottery table:
![Function overview](screenshot2.png)

## Lambda lottery function: 
![Function overview](screenshot3.png)





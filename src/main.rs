use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use std::error::Error as StdError; 


#[derive(Debug, Serialize, Deserialize)]
pub struct LotteryDraw {
    pub lottery_id: String,
    pub prize: String,
}

#[derive(Debug, Serialize)]
struct FailureResponse {
    pub body: String,
}

impl std::fmt::Display for FailureResponse {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.body)
    }
}

impl StdError for FailureResponse {}

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let r_id = event.context.request_id;
    let mut rng = rand::thread_rng();
    let random_draw: u8 = rng.gen_range(0..=2); // 0 for $5, 1 for $10, and 2 for nothing
    let prize = match random_draw {
        0 => "$5",
        1 => "$10",
        _ => "Nothing",
    };

    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let lottery_draw = LotteryDraw {
        lottery_id: r_id.clone(),
        prize: String::from(prize),
    };

    let lottery_id_attr = AttributeValue::S(lottery_draw.lottery_id.clone());
    let prize_attr = AttributeValue::S(lottery_draw.prize.to_string());

    let _resp = client
        .put_item()
        .table_name("Lottery")
        .item("lottery_id", lottery_id_attr)
        .item("prize", prize_attr)
        .send()
        .await
        .map_err(|_err| FailureResponse {
            body: _err.to_string(),
        })?;

    let resp = Response {
        req_id: r_id,
        msg: format!("Result: You won {}.", prize),
    };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			
	cargo --version 			
	rustfmt --version			
	rustup --version			
	clippy-driver --version		

format:
	cargo fmt --quiet

watch:
	cargo lambda watch

build: 
	cargo lambda build --release 

deploy:
	cargo lambda deploy 

aws-invoke: 
	cargo lambda invoke --remote lambda_lottery --data-ascii '{ "command": "draw"}'

invoke:
	cargo lambda invoke --data-ascii '{ "command": "draw"}' 

all: format lint test 
